//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EventsAPI4._0.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class COMPANY
    {
        public COMPANY()
        {
            this.EVENTs = new HashSet<EVENT>();
        }
    
        public decimal COMPANYID { get; set; }
        public string COMPANYNAME { get; set; }
        public string EMAILADDRESS { get; set; }
        public string PHONENUMBER { get; set; }
    
        public virtual ICollection<EVENT> EVENTs { get; set; }
    }
}
