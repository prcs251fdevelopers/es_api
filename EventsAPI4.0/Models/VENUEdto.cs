﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventsAPI4._0.Models
{
    public class VENUEdto
    {
        public decimal VENUEID { get; set; }
        public string VENUENAME { get; set; }
        public string POSTCODE { get; set; }
        public string ADDRESSLINE1 { get; set; }
        public decimal? MAXIMUMCAPACITY { get; set; }
        public decimal? STANDINGSPACES { get; set; }
        public decimal? SEATINGSPACES { get; set; }
    }
}