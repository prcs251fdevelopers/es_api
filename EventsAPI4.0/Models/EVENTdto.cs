﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventsAPI4._0.Models
{
    public class EVENTdto
    {
        public decimal EVENTID { get; set; }
        public string EVENTNAME { get; set; }
        public string EVENTTYPE { get; set; }
        public decimal? VENUEID { get; set; }

        //Add in event times
        public string STARTTIME { get; set; }
        public string STARTDATE { get; set; }
        public string ENDTIME { get; set; }
        public string ENDDATE { get; set; }


        public string STANDINGPRICE { get; set; }
        public string SEATINGPRICE { get; set; }
        public decimal? AGERESTRICTION { get; set; }
        public string EVENTDESCRIPTION { get; set; }
        public decimal? COMPANYID { get; set; }
    }
}