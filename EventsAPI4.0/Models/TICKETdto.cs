﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventsAPI4._0.Models
{
    public class TICKETdto
    {
        public decimal TICKETID { get; set; }
        public string TICKETTYPE { get; set; }
        public decimal? EVENTID { get; set; }
        public decimal? USERID { get; set; }
    }
}