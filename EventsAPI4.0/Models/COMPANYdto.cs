﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventsAPI4._0.Models
{
    public class COMPANYdto
    {
        public decimal COMPANYID { get; set; }
        public string COMPANYNAME { get; set; }
        public string EMAILADDRESS { get; set; }
        public string PHONENUMBER { get; set; }
    }
}