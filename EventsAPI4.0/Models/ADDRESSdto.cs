﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventsAPI4._0.Models
{
    public class ADDRESSdto
    {
        public decimal ADDRESSID { get; set; }
        public string ADDRESSLINE { get; set; }
        public string POSTCODE { get; set; }
    }
}