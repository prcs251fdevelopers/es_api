﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventsAPI4._0.Models
{
    public class SYSTEMUSERdto
    {
        public decimal USERID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string EMAILADDRESS { get; set; }
        public string TELEPHONENUMBER { get; set; }
        public string DOB { get; set; }
        public decimal? ADDRESSID { get; set; }
        public decimal? LOGINID { get; set; }
        public string USERTYPE { get; set; }
    }
}