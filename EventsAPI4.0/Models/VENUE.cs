//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EventsAPI4._0.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class VENUE
    {
        public VENUE()
        {
            this.EVENTs = new HashSet<EVENT>();
        }
    
        public decimal VENUEID { get; set; }
        public string VENUENAME { get; set; }
        public string POSTCODE { get; set; }
        public string ADDRESSLINE1 { get; set; }
        public Nullable<decimal> MAXIMUMCAPACITY { get; set; }
        public Nullable<decimal> STANDINGSPACES { get; set; }
        public Nullable<decimal> SEATINGSPACES { get; set; }
    
        public virtual ICollection<EVENT> EVENTs { get; set; }
    }
}
