﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EventsAPI4._0.Models;

namespace EventsAPI4._0.Controllers
{
    public class TicketController : ApiController
    {
        private EventsEntities db = new EventsEntities();

        // GET api/Ticket
        public IQueryable<TICKETdto> GetTICKETs()
        {
            var tickets = from t in db.TICKETs
                          select new TICKETdto()
                          {
                              TICKETID = t.TICKETID,
                              TICKETTYPE = t.TICKETTYPE,
                              EVENTID = t.EVENTID,
                              USERID = t.USERID
                          };

            return tickets.OrderBy(t => t.TICKETID);
        }

        //// GET api/Ticket/5
        //[ResponseType(typeof(TICKET))]
        //public IHttpActionResult GetTICKET(decimal id)
        //{
        //    TICKET ticket = db.TICKETs.Find(id);
        //    if (ticket == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(ticket);
        //}

        // GET api/Ticket
        public List<TICKETdto> GetTICKETs(decimal id)
        {
            List<TICKETdto> returnedTickets = new List<TICKETdto>();
            TICKETdto gotTicket = new TICKETdto();

            foreach (TICKET t in db.TICKETs)
            {
                if (t.USERID == id) {
                    gotTicket.TICKETID = t.TICKETID;
                    gotTicket.TICKETTYPE = t.TICKETTYPE;
                    gotTicket.EVENTID = t.EVENTID;
                    gotTicket.USERID = t.USERID;
                    returnedTickets.Add(gotTicket);

                    gotTicket = new TICKETdto();
                }
            }

            return returnedTickets;
        }

        // PUT api/Ticket/5
        public IHttpActionResult PutTICKET(TICKET ticket)
        {
            decimal id = ticket.TICKETID;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ticket.TICKETID)
            {
                return BadRequest();
            }

            db.Entry(ticket).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TICKETExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Ticket
        [ResponseType(typeof(TICKETdto))]
        public IHttpActionResult PostTICKET(TICKET @ticket)
        {
            TICKETdto createdTicket = new TICKETdto();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TICKETs.Add(@ticket);
            db.SaveChanges();

            createdTicket.TICKETID = @ticket.TICKETID;
            createdTicket.TICKETTYPE = @ticket.TICKETTYPE;
            createdTicket.EVENTID = @ticket.EVENTID;
            createdTicket.USERID = @ticket.USERID;

            return CreatedAtRoute("DefaultApi", new { id = @ticket.TICKETID }, createdTicket);
        }

        // DELETE api/Ticket/5
        [ResponseType(typeof(TICKET))]
        public IHttpActionResult DeleteTICKET(decimal id)
        {
            TICKET ticket = db.TICKETs.Find(id);
            if (ticket == null)
            {
                return NotFound();
            }

            db.TICKETs.Remove(ticket);
            db.SaveChanges();

            return Ok(ticket);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TICKETExists(decimal id)
        {
            return db.TICKETs.Count(e => e.TICKETID == id) > 0;
        }
    }
}