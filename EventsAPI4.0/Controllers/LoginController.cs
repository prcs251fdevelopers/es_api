﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EventsAPI4._0.Models;

namespace EventsAPI4._0.Controllers
{
    public class LoginController : ApiController
    {
        private EventsEntities db = new EventsEntities();

        // GET api/Login
        public IQueryable<LOGINdto> GetLOGINs()
        {
            var logins = from l in db.LOGINs
                         select new LOGINdto()
                         {
                             LOGINID = l.LOGINID,
                             PASSWORD = l.PASSWORD
                         };
            
            return logins.OrderBy(l => l.LOGINID);
        }

        // GET api/Login/5
        [ResponseType(typeof(LOGINdto))]
        public IHttpActionResult GetLOGIN(decimal id)
        {
            LOGINdto loginToReturn = new LOGINdto();
            LOGIN @login = db.LOGINs.Find(id);
            if (@login == null)
            {
                return NotFound();
            }
            else
            {
                loginToReturn.LOGINID = @login.LOGINID;
                loginToReturn.PASSWORD = @login.PASSWORD;
            }

            return Ok(loginToReturn);
        }

        // GET api/Login?loginId=
        [ResponseType(typeof(LOGINdto))]
        public IHttpActionResult GetLOGINpassword(decimal loginId)
        {
            LOGINdto loginToReturn = new LOGINdto();
            LOGIN @login = db.LOGINs.Find(loginId);
            if (@login == null)
            {
                return NotFound();
            }
            else
            {
                loginToReturn.LOGINID = @login.LOGINID;
                loginToReturn.PASSWORD = @login.PASSWORD;
            }

            return Ok(loginToReturn);
        }

        // PUT api/Login/5
        public IHttpActionResult PutLOGIN(LOGIN login)
        {
            decimal id = login.LOGINID;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != login.LOGINID)
            {
                return BadRequest();
            }

            db.Entry(login).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LOGINExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Login
        [ResponseType(typeof(LOGINdto))]
        public IHttpActionResult PostLOGIN(LOGIN login)
        {
            LOGINdto createdLogin = new LOGINdto();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LOGINs.Add(login);
            db.SaveChanges();

            createdLogin.LOGINID = login.LOGINID;

            return CreatedAtRoute("DefaultApi", new { id = login.LOGINID }, createdLogin);
        }



        // DELETE api/Login/5
        [ResponseType(typeof(LOGIN))]
        public IHttpActionResult DeleteLOGIN(decimal id)
        {
            LOGIN login = db.LOGINs.Find(id);
            if (login == null)
            {
                return NotFound();
            }

            db.LOGINs.Remove(login);
            db.SaveChanges();

            return Ok(login);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LOGINExists(decimal id)
        {
            return db.LOGINs.Count(e => e.LOGINID == id) > 0;
        }
    }
}