﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EventsAPI4._0.Models;

namespace EventsAPI4._0.Controllers
{
    public class CompanyController : ApiController
    {
        private EventsEntities db = new EventsEntities();

        // GET api/Company
        public IQueryable<COMPANYdto> GetCOMPANies()
        {
            var companies = from c in db.COMPANies
                            select new COMPANYdto()
                            {
                                COMPANYID = c.COMPANYID,
                                COMPANYNAME = c.COMPANYNAME,
                                EMAILADDRESS = c.EMAILADDRESS,
                                PHONENUMBER = c.PHONENUMBER
                            };

            return companies.OrderBy(c => c.COMPANYNAME);
        }

        // GET api/Company/5
        [ResponseType(typeof(COMPANYdto))]
        public IHttpActionResult GetCOMPANY(decimal id)
        {
            COMPANYdto companyToReturn = new COMPANYdto();
            COMPANY @company = db.COMPANies.Find(id);
            if (@company == null)
            {
                return NotFound();
            }
            else
            {
                companyToReturn.COMPANYID = @company.COMPANYID;
                companyToReturn.COMPANYNAME = @company.COMPANYNAME;
                companyToReturn.EMAILADDRESS = @company.EMAILADDRESS;
                companyToReturn.PHONENUMBER = @company.PHONENUMBER;
            }

            return Ok(companyToReturn);
        }

        // GET api/COMPANY/Name
        [ResponseType(typeof(List<COMPANYdto>))]
        public IHttpActionResult GetCOMPANY(string name)
        {
            List<COMPANYdto> listOfResults = new List<COMPANYdto>();
            COMPANYdto companyToAdd = new COMPANYdto();
            foreach (COMPANY c in db.COMPANies)
            {
                if (c.COMPANYNAME.Contains(name.Trim().ToUpper()))
                {
                    companyToAdd.COMPANYID = c.COMPANYID;
                    companyToAdd.COMPANYNAME = c.COMPANYNAME;
                    companyToAdd.EMAILADDRESS = c.EMAILADDRESS;
                    companyToAdd.PHONENUMBER = c.PHONENUMBER;

                    listOfResults.Add(companyToAdd);
                    companyToAdd = new COMPANYdto();
                }
            }

            if (listOfResults.Count > 0)
            {
                return Ok(listOfResults);
            }
            else
            {
                return NotFound();
            }
        }

        // PUT api/Company/5
        public IHttpActionResult PutCOMPANY(COMPANY company)
        {
            decimal id = company.COMPANYID;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != company.COMPANYID)
            {
                return BadRequest();
            }

            db.Entry(company).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!COMPANYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Company
        [ResponseType(typeof(COMPANYdto))]
        public IHttpActionResult PostCOMPANY(COMPANY company)
        {
            COMPANYdto createdCompany = new COMPANYdto();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.COMPANies.Add(company);
            db.SaveChanges();

            createdCompany.COMPANYID = company.COMPANYID;
            return CreatedAtRoute("DefaultApi", new { id = company.COMPANYID }, createdCompany);
        }

        // DELETE api/Company/5
        [ResponseType(typeof(COMPANY))]
        public IHttpActionResult DeleteCOMPANY(decimal id)
        {
            COMPANY company = db.COMPANies.Find(id);
            if (company == null)
            {
                return NotFound();
            }

            db.COMPANies.Remove(company);
            db.SaveChanges();

            return Ok(company);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool COMPANYExists(decimal id)
        {
            return db.COMPANies.Count(e => e.COMPANYID == id) > 0;
        }
    }
}