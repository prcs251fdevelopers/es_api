﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EventsAPI4._0.Models;

namespace EventsAPI4._0.Controllers
{
    public class VenueController : ApiController
    {
        private EventsEntities db = new EventsEntities();

        // GET api/Venue
        public IQueryable<VENUEdto> GetVENUEs()
        {
            var venues = from v in db.VENUEs
                         select new VENUEdto()
                         {
                             VENUEID = v.VENUEID,
                             VENUENAME = v.VENUENAME,
                             POSTCODE = v.POSTCODE,
                             ADDRESSLINE1 = v.ADDRESSLINE1,
                             MAXIMUMCAPACITY = v.MAXIMUMCAPACITY,
                             STANDINGSPACES = v.STANDINGSPACES,
                             SEATINGSPACES = v.SEATINGSPACES
                         };

            return venues.OrderBy(v => v.VENUENAME);
        }

        // GET api/Venue/5
        [ResponseType(typeof(VENUE))]
        public IHttpActionResult GetVENUE(decimal id)
        {
            VENUEdto venueToReturn = new VENUEdto();
            VENUE @venue = db.VENUEs.Find(id);
            if (@venue == null)
            {
                return NotFound();
            }
            else
            {
                venueToReturn.VENUEID = @venue.VENUEID;
                venueToReturn.VENUENAME = @venue.VENUENAME;
                venueToReturn.POSTCODE = @venue.POSTCODE;
                venueToReturn.ADDRESSLINE1 = @venue.ADDRESSLINE1;
                venueToReturn.MAXIMUMCAPACITY = @venue.MAXIMUMCAPACITY;
                venueToReturn.STANDINGSPACES = @venue.STANDINGSPACES;
                venueToReturn.SEATINGSPACES = @venue.SEATINGSPACES;
            }

            return Ok(venueToReturn);
        }

        // GET api/Venue/NAME
        [ResponseType(typeof(List<VENUEdto>))]
        public IHttpActionResult GetEVENT(string name)
        {
            List<VENUEdto> listOfResults = new List<VENUEdto>();
            VENUEdto venueToAdd = new VENUEdto();
            foreach (VENUE v in db.VENUEs)
            {
                if (v.VENUENAME.Contains(name.Trim().ToUpper()))
                {
                    venueToAdd.VENUEID = v.VENUEID;
                    venueToAdd.VENUENAME = v.VENUENAME;
                    venueToAdd.POSTCODE = v.POSTCODE;
                    venueToAdd.ADDRESSLINE1 = v.ADDRESSLINE1;
                    venueToAdd.MAXIMUMCAPACITY = v.MAXIMUMCAPACITY;
                    venueToAdd.STANDINGSPACES = v.STANDINGSPACES;
                    venueToAdd.SEATINGSPACES = v.SEATINGSPACES;

                    listOfResults.Add(venueToAdd);
                    venueToAdd = new VENUEdto();
                }
            }

            if (listOfResults.Count > 0)
            {
                return Ok(listOfResults);
            }
            else
            {
                return NotFound();
            }
        }

        // PUT api/Venue/5
        public IHttpActionResult PutVENUE(VENUE venue)
        {

            decimal id = venue.VENUEID;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != venue.VENUEID)
            {
                return BadRequest();
            }

            db.Entry(venue).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VENUEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Venue
        [ResponseType(typeof(VENUE))]
        public IHttpActionResult PostVENUE(VENUE venue)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.VENUEs.Add(venue);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = venue.VENUEID }, venue);
        }

        // DELETE api/Venue/5
        [ResponseType(typeof(VENUE))]
        public IHttpActionResult DeleteVENUE(decimal id)
        {
            VENUE venue = db.VENUEs.Find(id);
            if (venue == null)
            {
                return NotFound();
            }

            db.VENUEs.Remove(venue);
            db.SaveChanges();

            return Ok(venue);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VENUEExists(decimal id)
        {
            return db.VENUEs.Count(e => e.VENUEID == id) > 0;
        }
    }
}