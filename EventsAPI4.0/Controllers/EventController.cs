﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EventsAPI4._0.Models;

namespace EventsAPI4._0.Controllers
{
    public class EventController : ApiController
    {
        private EventsEntities db = new EventsEntities();

        // GET api/Event
        public IQueryable<EVENTdto> GetEVENTs()
        {
            var events = from e in db.EVENTs
                         select new EVENTdto()
                         {
                             EVENTID = e.EVENTID,
                             EVENTNAME = e.EVENTNAME,
                             EVENTTYPE = e.EVENTTYPE,
                             VENUEID = e.VENUEID,
                             STARTTIME = e.STARTTIME,
                             STARTDATE = e.STARTDATE,
                             ENDTIME = e.ENDTIME,
                             ENDDATE = e.ENDDATE,
                             STANDINGPRICE = e.STANDINGPRICE,
                             SEATINGPRICE = e.SEATINGPRICE,
                             AGERESTRICTION = e.AGERESTRICTION,
                             EVENTDESCRIPTION = e.EVENTDESCRIPTION,
                             COMPANYID = e.COMPANYID
                         };

            //events.OrderBy(c => c.EVENTNAME);
            return events.OrderBy(e => e.EVENTNAME);
        }

        // GET api/Event/5
        [ResponseType(typeof(EVENTdto))]
        public IHttpActionResult GetEVENT(decimal id)
        {
            EVENTdto eventToReturn = new EVENTdto();
            EVENT @event = db.EVENTs.Find(id);
            if (@event == null)
            {
                return NotFound();
            }
            else
            {
                eventToReturn.EVENTID = @event.EVENTID;
                eventToReturn.EVENTNAME = @event.EVENTNAME;
                eventToReturn.EVENTTYPE = @event.EVENTTYPE;
                eventToReturn.VENUEID = @event.VENUEID;
                eventToReturn.STARTTIME = @event.STARTTIME;
                eventToReturn.STARTDATE = @event.STARTDATE;
                eventToReturn.ENDTIME = @event.ENDTIME;
                eventToReturn.ENDDATE = @event.ENDDATE;
                eventToReturn.STANDINGPRICE = @event.STANDINGPRICE;
                eventToReturn.SEATINGPRICE = @event.SEATINGPRICE;
                eventToReturn.AGERESTRICTION = @event.AGERESTRICTION;
                eventToReturn.EVENTDESCRIPTION = @event.EVENTDESCRIPTION;
                eventToReturn.COMPANYID = @event.COMPANYID;
            }

            return Ok(eventToReturn);
        }

        // GET api/Event/NAME
        [ResponseType(typeof(List<EVENTdto>))]
        public IHttpActionResult GetEVENT(string name)
        {
            List<EVENTdto> listOfResults = new List<EVENTdto>();
            EVENTdto eventToAdd = new EVENTdto();
            foreach (EVENT e in db.EVENTs)
            {
                if (e.EVENTNAME.Contains(name.Trim().ToUpper()))
                {
                    eventToAdd.EVENTID = e.EVENTID;
                    eventToAdd.EVENTNAME = e.EVENTNAME;
                    eventToAdd.EVENTTYPE = e.EVENTTYPE;
                    eventToAdd.VENUEID = e.VENUEID;
                    eventToAdd.STARTTIME = e.STARTTIME;
                    eventToAdd.STARTDATE = e.STARTDATE;
                    eventToAdd.ENDTIME = e.ENDTIME;
                    eventToAdd.ENDDATE = e.ENDDATE;
                    eventToAdd.STANDINGPRICE = e.STANDINGPRICE;
                    eventToAdd.SEATINGPRICE = e.SEATINGPRICE;
                    eventToAdd.AGERESTRICTION = e.AGERESTRICTION;
                    eventToAdd.EVENTDESCRIPTION = e.EVENTDESCRIPTION;
                    eventToAdd.COMPANYID = e.COMPANYID;

                    listOfResults.Add(eventToAdd);
                    eventToAdd = new EVENTdto();
                }
            }

            if (listOfResults.Count > 0)
            {
                return Ok(listOfResults);
            }
            else
            {
                return NotFound();
            }
        }

        // PUT api/Event/5
        public IHttpActionResult PutEVENT(EVENT @event)
        {
            decimal id = @event.EVENTID;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != @event.EVENTID)
            {
                return BadRequest();
            }

            db.Entry(@event).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EVENTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Event
        [ResponseType(typeof(EVENTdto))]
        public IHttpActionResult PostEVENT(EVENT @event)
        {
            EVENTdto createdEvent = new EVENTdto();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EVENTs.Add(@event);
            db.SaveChanges();

            createdEvent.EVENTID = @event.EVENTID;

            return CreatedAtRoute("DefaultApi", new { id = @event.EVENTID }, createdEvent);
        }

        // DELETE api/Event/5
        [ResponseType(typeof(EVENT))]
        public IHttpActionResult DeleteEVENT(decimal id)
        {
            EVENT @event = db.EVENTs.Find(id);
            if (@event == null)
            {
                return NotFound();
            }

            db.EVENTs.Remove(@event);
            db.SaveChanges();

            return Ok(@event);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EVENTExists(decimal id)
        {
            return db.EVENTs.Count(e => e.EVENTID == id) > 0;
        }
    }
}