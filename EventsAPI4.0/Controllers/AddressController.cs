﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EventsAPI4._0.Models;

namespace EventsAPI4._0.Controllers
{
    public class AddressController : ApiController
    {
        private EventsEntities db = new EventsEntities();

        // GET api/Address
        public IQueryable<ADDRESSdto> GetADDRESSes()
        {
            var addreses = from a in db.ADDRESSes
                           select new ADDRESSdto()
                           {
                               ADDRESSID = a.ADDRESSID,
                               ADDRESSLINE = a.ADDRESSLINE,
                               POSTCODE = a.POSTCODE
                           };

            return addreses.OrderBy(a => a.ADDRESSID);
        }

        // GET api/Address/5
        [ResponseType(typeof(ADDRESSdto))]
        public IHttpActionResult GetADDRESS(decimal id)
        {
            ADDRESSdto addressToReturn = new ADDRESSdto();
            ADDRESS @address = db.ADDRESSes.Find(id);
            if (@address == null)
            {
                return NotFound();
            }
            else
            {
                addressToReturn.ADDRESSID = @address.ADDRESSID;
                addressToReturn.ADDRESSLINE = @address.ADDRESSLINE;
                addressToReturn.POSTCODE = @address.POSTCODE;
            }

            return Ok(addressToReturn);
        }

        // PUT api/Address/5
        public IHttpActionResult PutADDRESS(ADDRESS address)
        {
            decimal id = address.ADDRESSID;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != address.ADDRESSID)
            {
                return BadRequest();
            }

            db.Entry(address).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ADDRESSExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Address
        [ResponseType(typeof(ADDRESSdto))]
        public IHttpActionResult PostADDRESS(ADDRESS address)
        {
            ADDRESSdto createdAddress = new ADDRESSdto();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ADDRESSes.Add(address);
            db.SaveChanges();

            createdAddress.ADDRESSID = address.ADDRESSID;

            return CreatedAtRoute("DefaultApi", new { id = address.ADDRESSID }, createdAddress);
        }

        // DELETE api/Address/5
        [ResponseType(typeof(ADDRESS))]
        public IHttpActionResult DeleteADDRESS(decimal id)
        {
            ADDRESS address = db.ADDRESSes.Find(id);
            if (address == null)
            {
                return NotFound();
            }

            db.ADDRESSes.Remove(address);
            db.SaveChanges();

            return Ok(address);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ADDRESSExists(decimal id)
        {
            return db.ADDRESSes.Count(e => e.ADDRESSID == id) > 0;
        }
    }
}