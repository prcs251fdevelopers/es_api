﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EventsAPI4._0.Models;

namespace EventsAPI4._0.Controllers
{
    public class SystemUserController : ApiController
    {
        private EventsEntities db = new EventsEntities();

        // GET api/SystemUser
        public IQueryable<SYSTEMUSERdto> GetSYSTEMUSERs()
        {
            var users = from u in db.SYSTEMUSERs
                        select new SYSTEMUSERdto()
                        {
                            USERID = u.USERID,
                            FIRSTNAME = u.FIRSTNAME,
                            LASTNAME = u.LASTNAME,
                            EMAILADDRESS = u.EMAILADDRESS,
                            TELEPHONENUMBER = u.TELEPHONENUMBER,
                            DOB = u.DOB,
                            ADDRESSID = u.ADDRESSID,
                            LOGINID = u.LOGINID,
                            USERTYPE = u.USERTYPE
                        };

            return users.OrderBy(u => u.LASTNAME);
        }

        // GET api/SystemUser/5
        [ResponseType(typeof(SYSTEMUSERdto))]
        public IHttpActionResult GetSYSTEMUSER(decimal id)
        {
            SYSTEMUSERdto userToReturn = new SYSTEMUSERdto();
            SYSTEMUSER @user = db.SYSTEMUSERs.Find(id);
            if (@user == null)
            {
                return NotFound();
            }
            else
            {
                userToReturn.USERID = @user.USERID;
                userToReturn.FIRSTNAME = @user.FIRSTNAME;
                userToReturn.LASTNAME = @user.LASTNAME;
                userToReturn.EMAILADDRESS = @user.EMAILADDRESS;
                userToReturn.TELEPHONENUMBER = @user.TELEPHONENUMBER;
                userToReturn.DOB = @user.DOB;
                userToReturn.ADDRESSID = @user.ADDRESSID;
                userToReturn.LOGINID = @user.LOGINID;
                userToReturn.USERTYPE = @user.USERTYPE;
            }

            return Ok(userToReturn);
        }

        // GET api/Systemuser/Email
        [ResponseType(typeof(SYSTEMUSERdto))]
        public IHttpActionResult GetSYSTEMUSER(string email)
        {
            SYSTEMUSERdto retrievedUser = new SYSTEMUSERdto();
            foreach (SYSTEMUSER s in db.SYSTEMUSERs)
            {
                if (s.EMAILADDRESS.Equals(email.ToUpper()))
                {
                    retrievedUser.USERID = s.USERID;
                    retrievedUser.FIRSTNAME = s.FIRSTNAME;
                    retrievedUser.LASTNAME = s.LASTNAME;
                    retrievedUser.EMAILADDRESS = s.EMAILADDRESS;
                    retrievedUser.TELEPHONENUMBER = s.TELEPHONENUMBER;
                    retrievedUser.DOB = s.DOB;
                    retrievedUser.ADDRESSID = s.ADDRESSID;
                    retrievedUser.LOGINID = s.LOGINID;
                    retrievedUser.USERTYPE = s.USERTYPE;

                    return Ok(retrievedUser);
                }
            }

            return NotFound();
        }

        // GET api/SYSTEMUSER/firstName lastName
        [ResponseType(typeof(List<SYSTEMUSERdto>))]
        public IHttpActionResult GetSYSTEMUSER(string firstName, string lastName)
        {
            List<SYSTEMUSERdto> listOfResults = new List<SYSTEMUSERdto>();
            SYSTEMUSERdto userToAdd = new SYSTEMUSERdto();
            foreach (SYSTEMUSER s in db.SYSTEMUSERs)
            {
                if (s.FIRSTNAME.Contains(firstName.Trim().ToUpper()) &&
                    s.LASTNAME.Contains(lastName.Trim().ToUpper()))
                {
                    userToAdd.USERID = s.USERID;
                    userToAdd.FIRSTNAME = s.FIRSTNAME;
                    userToAdd.LASTNAME = s.LASTNAME;
                    userToAdd.EMAILADDRESS = s.EMAILADDRESS;
                    userToAdd.TELEPHONENUMBER = s.TELEPHONENUMBER;
                    userToAdd.DOB = s.DOB;
                    userToAdd.ADDRESSID = s.ADDRESSID;
                    userToAdd.LOGINID = s.LOGINID;
                    userToAdd.USERTYPE = s.USERTYPE;

                    listOfResults.Add(userToAdd);
                    userToAdd = new SYSTEMUSERdto();
                }
            }

            if (listOfResults.Count > 0)
            {
                return Ok(listOfResults);
            }
            else
            {
                return NotFound();
            }
        }

        // PUT api/SystemUser/5
        public IHttpActionResult PutSYSTEMUSER(SYSTEMUSER systemuser)
        {
            decimal id = systemuser.USERID;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != systemuser.USERID)
            {
                return BadRequest();
            }

            db.Entry(systemuser).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SYSTEMUSERExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/SystemUser
        [ResponseType(typeof(SYSTEMUSERdto))]
        public IHttpActionResult PostSYSTEMUSER(SYSTEMUSER systemuser)
        {
            SYSTEMUSERdto createdUser = new SYSTEMUSERdto();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SYSTEMUSERs.Add(systemuser);
            db.SaveChanges();

            createdUser.USERID = systemuser.USERID;
            createdUser.FIRSTNAME = systemuser.FIRSTNAME;
            createdUser.LASTNAME = systemuser.LASTNAME;
            createdUser.EMAILADDRESS = systemuser.EMAILADDRESS;
            createdUser.TELEPHONENUMBER = systemuser.TELEPHONENUMBER;
            createdUser.DOB = systemuser.DOB;
            createdUser.ADDRESSID = systemuser.ADDRESSID;
            createdUser.LOGINID = systemuser.LOGINID;
            createdUser.USERTYPE = systemuser.USERTYPE;

            return CreatedAtRoute("DefaultApi", new { id = systemuser.USERID }, createdUser);
        }

        // DELETE api/SystemUser/5
        [ResponseType(typeof(SYSTEMUSER))]
        public IHttpActionResult DeleteSYSTEMUSER(decimal id)
        {
            SYSTEMUSER systemuser = db.SYSTEMUSERs.Find(id);
            if (systemuser == null)
            {
                return NotFound();
            }

            db.SYSTEMUSERs.Remove(systemuser);
            db.SaveChanges();

            return Ok(systemuser);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SYSTEMUSERExists(decimal id)
        {
            return db.SYSTEMUSERs.Count(e => e.USERID == id) > 0;
        }
    }
}